export const getPokemonDetail = async (pokemonName) => {
  const response = await fetch(
    `https://pokeapi.co/api/v2/pokemon/${pokemonName}`
  );
  const jsonResponse = await response.json();
  return jsonResponse;
};

export const getPokemonList = async (pokemonName) => {
  const response = await fetch(
    "https://pokeapi.co/api/v2/pokemon?limit=10&offset=0"
  );
  const jsonResponse = await response.json();
  return jsonResponse;
};
