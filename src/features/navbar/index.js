import { useEffect, useState } from "react";

export const Navbar = ({ listaDeCapturados }) => {
  const [numPokemonsCapturados, setNumPokemonsCapturados] = useState(0);
  useEffect(() => {
    setNumPokemonsCapturados(listaDeCapturados.length);
  }, [listaDeCapturados]);

  return (
    <div style={{ border: "1px solid black" }}>
      Contador de Pokemons: {numPokemonsCapturados}
    </div>
  );
};
