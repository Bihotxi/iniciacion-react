import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import { PokemonList } from "../features/pokemonList";
import { PokemonDetails } from "../features/pokemonDetail";

export const Router = (props) => (
  <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PokemonList {...props} />}></Route>
        <Route
          path="/:pokemonName"
          element={<PokemonDetails {...props} />}></Route>
      </Routes>
    </BrowserRouter>
  </>
);
