import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getPokemonDetail } from "../../services/pokemon";
import { Modal } from "../../components/Modal";
import ReactDOM from "react-dom";

export const PokemonDetails = ({ listaDeCapturados, setListaDeCapturados }) => {
  const params = useParams();
  const navigate = useNavigate();
  const [pokemonInfo, setPokemonInfo] = useState();
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    getPokemonDetail(params.pokemonName).then((data) => {
      setPokemonInfo(data);
    });
  }, [params.pokemonName]);

  return pokemonInfo ? (
    <div>
      <div>
        <button onClick={() => navigate("/")}>Atrás</button>
        <button
          disabled={listaDeCapturados.includes(params.pokemonName)}
          onClick={() =>
            setListaDeCapturados([...listaDeCapturados, params.pokemonName])
          }>
          Capturado
        </button>
      </div>
      {params.pokemonName}
      <div>
        <img
          src={pokemonInfo?.sprites.front_default}
          alt={pokemonInfo?.name}></img>
      </div>
      <button onClick={() => setModalVisible(true)}>Mostrar Modal</button>
      {modalVisible &&
        ReactDOM.createPortal(
          <Modal
            title="Título de la modal"
            description="Descripción de la modal"
            onClickClose={() => {
              setModalVisible(false);
            }}
          />,
          document.querySelector("#modal")
        )}
    </div>
  ) : (
    <div>Loading...</div>
  );
};
