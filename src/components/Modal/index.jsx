export const Modal = ({ title, description, onClickClose }) => {
  return (
    <div
      style={{
        position: "absolute",
        width: "100vw",
        height: "100vh",
        top: "0",
        backgroundColor: "white",
        zIndex: "100",
      }}>
      <p>{title}</p>
      <p>{description}</p>
      <button onClick={onClickClose}>Cerrar</button>
    </div>
  );
};
