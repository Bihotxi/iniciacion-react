import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { styles } from "./styles";
import { getPokemonList as getPokemonListApi } from "../../services/pokemon";

export const PokemonList = ({ listaDeCapturados, setListaDeCapturados }) => {
  const navigate = useNavigate();
  const [pokemonListData, setPokemonListData] = useState();

  const getPokemonListData = useCallback(() => {
    const getPokemonList = async () => {
      try {
        const response = await getPokemonListApi();
        setPokemonListData(response.results);
      } catch (error) {
        console.error("Error en la petición del listado de Pokemons", error);
      }
    };

    getPokemonList();
  }, []);

  useEffect(getPokemonListData, [getPokemonListData]);

  const capturarPokemons = (pokemonName) =>
    setListaDeCapturados([...listaDeCapturados, pokemonName]);
  return (
    <div style={styles.table}>
      {pokemonListData?.map((pokemon) => (
        <div key={pokemon.name} style={styles.listItems}>
          {pokemon.name.toUpperCase()}
          <div>
            <button onClick={() => navigate(`/${pokemon.name}`)}>
              Detalle
            </button>
            <button
              disabled={listaDeCapturados.includes(pokemon.name)}
              onClick={() => capturarPokemons(pokemon.name)}>
              Capturado
            </button>
          </div>
        </div>
      ))}
    </div>
  );
};
