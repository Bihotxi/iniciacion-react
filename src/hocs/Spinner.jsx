import { useEffect, useState } from "react";

export const Spinner = ({ prom, children: Children }) => {
  const [promSolved, setPromSolved] = useState();

  useEffect(() => {
    (async () => {
      const promiseSolved = await prom;
      setPromSolved(promiseSolved);
    })();
  }, [prom]);

  return (
    <div className="Spinner">
      {promSolved ? <Children promSolved={promSolved} /> : <div>Loading</div>}
      {}
    </div>
  );
};
