import { useState } from "react";
import "./App.css";
import { Navbar } from "./features/navbar";
import { Router } from "./routes/Router";

function App() {
  const [listaDeCapturados, setListaDeCapturados] = useState([]);

  const leerListaCapturados = () => {
    if (!listaDeCapturados?.length && localStorage.listaDeCapturados?.length) {
      const parseList = JSON.parse(localStorage.listaDeCapturados);
      setListaDeCapturados(parseList);
      return parseList;
    }
    return listaDeCapturados;
  };

  const sobreescribirListaCapturados = (nuevaLista) => {
    localStorage.listaDeCapturados = JSON.stringify(nuevaLista);
    setListaDeCapturados(nuevaLista);
  };
  return (
    <div className="App">
      <Navbar listaDeCapturados={leerListaCapturados()} />
      <Router
        listaDeCapturados={leerListaCapturados()}
        setListaDeCapturados={sobreescribirListaCapturados}
      />
    </div>
  );
}

export default App;
